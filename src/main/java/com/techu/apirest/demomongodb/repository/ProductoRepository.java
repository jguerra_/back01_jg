package com.techu.apirest.demomongodb.repository;

import com.techu.apirest.demomongodb.model.ProductoModel;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {
}
