package com.techu.apirest.demomongodb.model;

import java.util.UUID;

public class ProductoModelSinId {

    private String descripcion;
    private Double precio;

    public ProductoModelSinId() {}

    public ProductoModelSinId(String descripcion, Double precio) {
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public ProductoModel convert (ProductoModelSinId origin){
        UUID uuid = UUID.randomUUID();
        return new ProductoModel(uuid.toString(),origin.getDescripcion(),origin.getPrecio());
    }
}
