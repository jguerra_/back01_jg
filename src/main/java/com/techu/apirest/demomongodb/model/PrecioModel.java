package com.techu.apirest.demomongodb.model;

public class PrecioModel {
    private Double precio;

    public PrecioModel(){}

    public PrecioModel(Double precio) {
        this.precio = precio;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}
