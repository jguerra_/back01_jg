package com.techu.apirest.demomongodb.controller;

import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import com.techu.apirest.demomongodb.component.AuthHandler;
import com.techu.apirest.demomongodb.component.JWTBuilder;
import com.techu.apirest.demomongodb.model.PrecioModel;
import com.techu.apirest.demomongodb.model.ProductoModel;
import com.techu.apirest.demomongodb.model.ProductoModelSinId;
import com.techu.apirest.demomongodb.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductoController {
    @Autowired
    ProductoService productoService;
    @Autowired
    JWTBuilder jwtBuilder;

    @GetMapping("/tokenget")
    public String tokenget(@RequestParam(value="nombre", defaultValue="Tech U!") String name){
        return jwtBuilder.generateToken(name,"admin");
    }

    @GetMapping(path = "/productos",headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    @GetMapping(path = "/productos/{id}", headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public Optional<ProductoModel> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }


    @PostMapping(path = "/productos", headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public ResponseEntity<String> postProductos (@RequestBody ProductoModelSinId newProducto){
        productoService.save(newProducto.convert(newProducto));
        return new ResponseEntity<>("Product created successfully",HttpStatus.NO_CONTENT);
    }

    @PutMapping(path = "/productos", headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    ResponseEntity<ProductoModel> putProductos(@RequestBody ProductoModel productToUpdate){
        Optional<ProductoModel> pr = productoService.findById(productToUpdate.getId());
        if(pr.isPresent()) {
            return new ResponseEntity<>(productoService.save(productToUpdate), HttpStatus.ACCEPTED);
        }else{
            return new ResponseEntity<>(new ProductoModel("Error","No se ha encontrado el objeto a modificar",0.0), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(path = "/productos/{id}", headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    ResponseEntity<String> deleteProductos(@PathVariable String id){
        Boolean prd = productoService.deleteById(id);
        if(prd == true){
            return new ResponseEntity<>("Asset deleted successfully!",HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Error while performing deletion. Asset not deleted or already doesn't exist",HttpStatus.FORBIDDEN);
        }
    }

    @PatchMapping(path = "/productos/{id}", headers = {"Authorizations"})
    @Before(@BeforeElement(AuthHandler.class))
    ResponseEntity<String> updatePrecio(@PathVariable String id, @RequestBody PrecioModel preciotoU) {
        Optional<ProductoModel> pr = productoService.findById(id);
        if (pr.isPresent()){
            ProductoModel pru = pr.get();
            pru.setPrecio(preciotoU.getPrecio());
            return new ResponseEntity<>(productoService.save(pru).toString(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Asset not patched due it doesn't exist",HttpStatus.NOT_FOUND);
        }
    }
}
