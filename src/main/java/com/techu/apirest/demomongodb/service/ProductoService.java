package com.techu.apirest.demomongodb.service;

import com.techu.apirest.demomongodb.model.ProductoModel;
import com.techu.apirest.demomongodb.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {
    @Autowired
    ProductoRepository productoRepository;

    public List<ProductoModel> findAll() {
        return productoRepository.findAll();
    }

    public Optional<ProductoModel> findById(String id) {
        return productoRepository.findById(id);
    }

    public ProductoModel save(ProductoModel producto) {
        return productoRepository.save(producto);
    }


    public boolean delete(ProductoModel entity) {
        try {
            productoRepository.delete(entity);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }

    public boolean deleteById(String id) {
        try {
            productoRepository.deleteById(id);
            return true;
        } catch (Exception e){
            return false;
        }
    }


}
