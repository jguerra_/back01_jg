package com.techu.apirest.demomongodb.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductoModelSinIdTest {

    @Test
    void convert() {
        new ProductoModelSinId();
    }

    @Test
    void getDescripcion(){
        ProductoModelSinId tst = new ProductoModelSinId("Descripcion",20.0);
        assertEquals("Descripcion",tst.getDescripcion());
    }
}